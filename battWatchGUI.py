# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 08:44:40 2020

@author: Alexander Figl
@name: battWatchGUI

"""
# libs

PATH = "\\\\PCINS7\Daten\DATALOGGER\SnowcatcherGalerie.dat"

import numpy as np
import pandas as pd
import tkinter as tk
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from datetime import datetime as dt
import requests
import urllib.request
from astropy.table import QTable, Table

def main():

    def connect():
        
        host='http://google.com'
        try:
            urllib.request.urlopen(host) #Python 3.x
            return True
        except:
            return False
        
    city = "Neustift"
    bgcolor = '#cfcfcf'
        
    # GUI with tikinter
    root = tk.Tk()
    root.wm_title("battWatchGUI")

    labelPlot = tk.Label(root, height=10, width=130, bd=15 , bg=bgcolor)
    labelPlot.pack(fill=tk.BOTH, side=tk.BOTTOM)
    
    labelDays = tk.Label(labelPlot, text="Days", bg=bgcolor, font=('Courier',10))
    labelDays.place(relx=0, rely=0, relwidth=0.1, relheight=0.2)
    
    entryDays = tk.Entry(labelPlot, font=('Courier',10))
    entryDays.place(relx=0.1, rely=0.0, relwidth=0.25, relheight=0.2)
    
    labelIntervall = tk.Label(labelPlot, text="Intervall", bg=bgcolor, font=('Courier',10))
    labelIntervall.place(relx=0, rely=0.25, relwidth=0.1, relheight=0.2)
    
    entryIntervall = tk.Entry(labelPlot, font=('Courier',10))
    entryIntervall.place(relx=0.1, rely=0.25, relwidth=0.25, relheight=0.2)
    
    if connect():
        # get weather forecast from API
        weather_key = 'b99cb5e0d9b9522e60369d510ca63d8e'
        url = 'https://api.openweathermap.org/data/2.5/forecast'
        params = {'appid': weather_key, 'q':city, 'units':'metric'}
        response = requests.get(url, params=params)
        weather = response.json()
        labelForecast = tk.Label(labelPlot, text=format_response(weather) , bd=25 ,font=('Courier',10))
        labelForecast.place(relx=0.375, rely=0, relwidth=0.625, relheight=1)
        labelForecast.config(highlightbackground='white')
    else:
        labelForecast = tk.Label(labelPlot, text='forecast not possible, no internet connection' , bd=25 ,font=('Courier',10))
        labelForecast.place(relx=0.375, rely=0, relwidth=0.625, relheight=1)
        labelForecast.config(highlightbackground='white')
        
    buttonClear = tk.Button(labelPlot, text="Clear Graph", font=('Courier',8))
    buttonClear.place(relx=0, rely=0.55, relwidth=0.35, relheight=0.2)
    
    buttonPlot = tk.Button(labelPlot, text="Plot Graph", font=('Courier',8), command=lambda: plotGraph(root, buttonClear, entryDays.get(),entryIntervall.get()))
    buttonPlot.place(relx=0, rely=0.8, relwidth=0.35, relheight=0.2)
    
    root.mainloop()

    input()
    
    return root, buttonClear

def plotGraph(root, buttonClear, entryDays, daysIntervall):
    
    # variables
    data            = pd.read_csv(PATH) # read csv file
    date_ticks      = [] # create array for x-axis
    date_ticks_list = [] # create list for x-axis
    daysBack        = int(entryDays) # choose how many days back you want to see the charge state
    intervall       = round((96//int(daysIntervall)))
    intervall       = int(intervall) # every n-th intervall should be shown on the x-axis. data is saved every 15min, so for e.g. 24h format choose 96 intervalls
    data_length     = len(data) 
    start           = data_length-(96*daysBack) # first intervall from csv
    # get data
    batt            = data.iloc[start::, 4] # read out the battery values into dataframePlot
    ydt             = data.iloc[start::intervall, 1:4] # read out the year date and time into dataframePlot
    ydt             = ydt.reset_index() # reset the index of the dataframePlot

    j               = len(ydt) # var for loop reps   
    
    
    for i in range(int(j)):
        date_ticks = str(ydt.iloc[i,1]) + str(ydt.iloc[i,2]) + str(ydt.iloc[i,3]) # convert ydt etries into strings, stick them together and write it into date_ticks
        date_ticks = dt.strptime(date_ticks,'%Y%j%H%M') # use dt.strptime to convert string into formated date and time
        date_ticks_list.append(str(date_ticks)) # convert the entries into strings again and write it in a list for the x-axis
        

    # plot
    fig = plt.figure()  # create a figure object
    plt.close()
    ax = fig.add_subplot(111)  # create an axes object in the figure
    ax.set_ylabel('battery / V')
    ax.set_xticks(np.arange(start,data_length,intervall))
    ax.set_xticklabels(date_ticks_list)
    for tick in ax.get_xticklabels():
        tick.set_rotation(30)
    ax.grid()
    
    fig.tight_layout()
    ax.plot(batt)
    
    canvas = FigureCanvasTkAgg(fig, master=root)  
    canvas.draw()
    canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

    toolbar = NavigationToolbar2Tk(canvas, root)
    toolbar.update()
    canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
    
    buttonClear['command'] = lambda: remove_plot(canvas,toolbar)
    
def remove_plot(canvas,toolbar):
    canvas.get_tk_widget().destroy()
    toolbar.destroy()
    
# format API information    
def format_response(weather):
    
    F_dateList = []
    F_desList = []
    
    try:
        for i in range(5):
            date = weather['list'][i]['dt_txt']
            F_dateList.append(date)
            description = weather['list'][i]['weather'][0]['description']
            F_desList.append(description)
            
        t = QTable()
        t['weather_date'] = F_dateList
        t['weather'] = F_desList
        
        final_str = Table([t['weather_date'], t['weather']])
        
    except:
        final_str = 'There was a problem retrieveing that inforamtion'
        
    return final_str

    
if __name__ == "__main__": 
    main()